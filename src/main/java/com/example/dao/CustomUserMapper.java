package com.example.dao;

import com.example.model.User;

import java.util.List;

public interface CustomUserMapper {
    List<User> findUserList();

}
