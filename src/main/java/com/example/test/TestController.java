package com.example.test;

import com.example.dao.CustomUserMapper;
import com.example.model.User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
public class TestController {
    @Resource
    CustomUserMapper customUserMapper;

    @GetMapping("/test1")
    public List<User> test1() {
        return customUserMapper.findUserList();
    }
}
