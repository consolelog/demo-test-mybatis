package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@tk.mybatis.spring.annotation.MapperScan(basePackages = {"com.example.dao"})
public class DemoTestMybatisApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoTestMybatisApplication.class, args);
    }

}
