create database if not exists `test`;

use `test`;

create table if not exists `t_order`
(
    id   int auto_increment
        primary key,
    name varchar(200) null
);

create table if not exists `t_user`
(
    id       int auto_increment
        primary key,
    username varchar(200) null,
    password varchar(200) null
);

insert into t_order (name) value ('111'),('222');
insert into t_user (username, password) value ('111', '111'), ('222', '222');
